# 🎯 Shhhhhhlime 🎯

Shooter 3D en tercera persona con temática de sigilo.  

## 📄 Descripción
Proyecto realizado para la asignatura "Videojuegos" desarrollado durante el segundo año de Grado Superior de Desarrollo de Aplicaciones Multiplataforma + Perfil videojuegos y ocio digital (DAMvi).

## 🎮 Controles
> Saltar:  [Espacio].

> Agacharse: [Shift].

> Disparar: Apuntar con el mouse y [click izquierdo].

> Equipar otra arma: [Click derecho].

> Hackear cámaras: Apuntar con el mouse y mantener pulsada la [F].

> Mover cámara horizontalmente: izquierda con [Q] y derecha con [E].

## 💻 Tecnologías
![C#](https://img.shields.io/badge/c%23-%23239120.svg?style=for-the-badge&logo=csharp&logoColor=white)
![Unity](https://img.shields.io/badge/unity-%23000000.svg?style=for-the-badge&logo=unity&logoColor=white)
- Dot product (backstab).
- State machine.
- Navmesh (enemies).
- Ragdoll.
- Shaders.
- VFX.
- Trail renderer (shoot).
- Raycasts.

## 📽️ Gameplay (clic en la imagen para ver el vídeo)
[![](http://img.youtube.com/vi/OQuQSC7PGvE/0.jpg)](http://www.youtube.com/watch?v=OQuQSC7PGvE "Shhhhhhlime")


## ©️ Desarrolladores
- Ethan Corchero Martín.
- Ana María Gómez León.
- Selene Milanés Rodríguez.

🎶 Música: _Stealth Mission_ por Ihor Koliako.
