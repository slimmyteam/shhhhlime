using UnityEngine;

[CreateAssetMenu(fileName = "GameEvent", menuName = "GameEvent/GameObjectInt")]
public class GEGenericoGameObjectInt : GEGenerico<GameObject, int> { }
