using UnityEngine;

[CreateAssetMenu(fileName = "GameEvent", menuName = "GameEvent/GameObject")]
public class GEGenericoGameObject : GEGenerico<GameObject> { }
