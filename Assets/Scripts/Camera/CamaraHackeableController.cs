using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;

public class CamaraHackeableController : MonoBehaviour
{
    [Header("Referencias")]
    [SerializeField]
    private Camera m_Camera;

    [Header("Shader Camara Hackeable")]
    [SerializeField]
    private UniversalRendererData m_UniversalRendererData;
    List<ScriptableRendererFeature> m_RendererFeatures;

    private void Awake()
    {
        m_RendererFeatures = m_UniversalRendererData.rendererFeatures;
    }

    public void ActivarDesactivarCamara()
    {
        m_Camera.gameObject.SetActive(!m_Camera.gameObject.activeInHierarchy);
        m_RendererFeatures[2].SetActive(!m_RendererFeatures[2].isActive);
    }

}
