using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.InputSystem;
using UnityEngine.Rendering.Universal;

public class CameraController : MonoBehaviour
{
    [Header("Inputs")]
    [SerializeField]
    private InputActionAsset m_InputActionAsset;
    private InputActionAsset m_Input;
    private InputAction m_MovementLeft, m_MovementRight;
    private InputAction m_VerticalMovement;

    [SerializeField]
    private Transform m_Player;

    [Header("Camara")]
    [SerializeField]
    private float m_VelocidadRotacion = 180f;
    private float m_RotacionCamara = 0f;
    [SerializeField]
    private float m_DistanciaDesdeJugador = 5f;
    private bool isRotatingLeft, isRotatingRight;

    [Header("Parametros")]
    [SerializeField]
    private float m_Sensitivity;
    [SerializeField]
    private float m_MinVerticalRotation;
    [SerializeField]
    private float m_MaxVerticalRotation;
    [SerializeField]
    private float m_VerticalRotation = 0;

    //[Header("Delegados")]
    public event Action NewForwardDirection;

    [Header("Shader Camara Muerte")]
    [SerializeField]
    private UniversalRendererData m_UniversalRendererData;
    List<ScriptableRendererFeature> m_RendererFeatures;
    [SerializeField]
    private Material materialCamaraMuerte;
    private int vignettePowerID;
    [SerializeField]
    private float tiempoCamaraMuerte = 0.3f;

    //-------------------------------[ FUNCIONES UNITY ]-------------------------------//
    private void Awake()
    {
        Assert.IsNotNull(m_InputActionAsset);

        m_Input = Instantiate(m_InputActionAsset);
        m_MovementLeft = m_Input.FindActionMap("Default").FindAction("CameraMovementLeft");
        m_MovementLeft.started += MovimientoIzquierdaStart;
        m_MovementLeft.canceled += MovimientoIzquierdaStop;
        m_MovementRight = m_Input.FindActionMap("Default").FindAction("CameraMovementRight");
        m_MovementRight.started += MovimientoDerechaStart;
        m_MovementRight.canceled += MovimientoDerechaStop;
        m_VerticalMovement = m_Input.FindActionMap("Default").FindAction("CameraVerticalMovement");
        m_Input.FindActionMap("Default").Enable();

        //Shader camara de la muerte
        m_RendererFeatures = new List<ScriptableRendererFeature>();
        m_RendererFeatures = m_UniversalRendererData.rendererFeatures;
        m_RendererFeatures[1].SetActive(false);
        m_RendererFeatures[2].SetActive(false); //shader camara hackeable
        vignettePowerID = Shader.PropertyToID("_VignettePower");
        materialCamaraMuerte.SetFloat(vignettePowerID, 1.89f);

        isRotatingLeft = false;
        isRotatingRight = false;

        RotarCamara(1);
    }

    private void Update()
    {
        if (isRotatingLeft)
        {
            RotarCamara(-1);
        }
        else if (isRotatingRight)
        {
            RotarCamara(1);
        }

        if (m_VerticalMovement.ReadValue<Vector2>().y != 0)
        {
            m_VerticalRotation -= m_VerticalMovement.ReadValue<Vector2>().y * m_Sensitivity * Time.deltaTime;
            m_VerticalRotation = Mathf.Clamp(m_VerticalRotation, m_MinVerticalRotation, m_MaxVerticalRotation);
            transform.localEulerAngles = new Vector3(m_VerticalRotation, transform.localEulerAngles.y, transform.localEulerAngles.z);
        }

        // Camara sigue al jugador y mantiene una distancia
        transform.position = m_Player.position - transform.forward * m_DistanciaDesdeJugador;
    }

    //-------------------------------[ INPUTS ]-------------------------------//
    private void MovimientoIzquierdaStart(InputAction.CallbackContext actionContext)
    {
        isRotatingLeft = true;
        NewForwardDirection.Invoke();
    }

    private void MovimientoIzquierdaStop(InputAction.CallbackContext actionContext)
    {
        isRotatingLeft = false;
        NewForwardDirection.Invoke();
    }

    private void MovimientoDerechaStart(InputAction.CallbackContext actionContext)
    {
        isRotatingRight = true;
        NewForwardDirection.Invoke();
    }

    private void MovimientoDerechaStop(InputAction.CallbackContext actionContext)
    {
        isRotatingRight = false;
        NewForwardDirection.Invoke();
    }

    //-------------------------------[ FUNCIONES ]-------------------------------//
    private void RotarCamara(float direccion)
    {
        //Rotamos la "y" de la camara
        m_RotacionCamara += direccion * m_VelocidadRotacion * Time.deltaTime;
        //Hacemos que la camara mantenga la "x" que ya tenia y la hacemos rotar en "y"
        transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, m_RotacionCamara, 0);
    }

    //-------------------------------[ EVENTOS ]-------------------------------//
    public void IniciarCorutinaCamaraDeLaMuerte()
    {
        m_RendererFeatures[1].SetActive(true);
        StartCoroutine(ActivarCamaraDeLaMuerte());
    }

    //-------------------------------[ CORUTINAS ]-------------------------------//
    public IEnumerator ActivarCamaraDeLaMuerte()
    {
        while (materialCamaraMuerte.GetFloat(vignettePowerID) - tiempoCamaraMuerte > 0)
        {
            materialCamaraMuerte.SetFloat(vignettePowerID, materialCamaraMuerte.GetFloat(vignettePowerID) - tiempoCamaraMuerte);

            yield return new WaitForSeconds(tiempoCamaraMuerte);
        }
        materialCamaraMuerte.SetFloat(vignettePowerID, 0);
    }

    private void OnDestroy()
    {
        m_RendererFeatures[1].SetActive(false);
    }

}
