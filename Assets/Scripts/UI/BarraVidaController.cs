using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class BarraVidaController : MonoBehaviour
{
    [Header("Parámetros Barra Vida")]
    [SerializeField]
    private Gradient m_gradient;
    private float m_maxHp;
    private float m_currentHp;
    private float m_lastHp;
    [SerializeField]
    private float m_lerpTime;

    [Header("Scriptable objects")]
    [SerializeField]
    private SOPlayer m_SOPlayer;

    [Header("Referencias a objetos")]
    [SerializeField]
    private Image m_fill;

    void Start()
    {
        m_maxHp = m_SOPlayer.maxHP;
        m_currentHp = m_SOPlayer.hp;
        m_fill.fillAmount = m_currentHp / m_maxHp;
        m_fill.color = m_gradient.Evaluate(m_fill.fillAmount);
    }
    private IEnumerator ActualitzarVida()
    {
        float currentTime = 0f;
        while (currentTime <= m_lerpTime)
        {
            m_fill.fillAmount = Mathf.Lerp(m_lastHp / m_maxHp, m_currentHp / m_maxHp, currentTime / m_lerpTime);
            m_fill.color = m_gradient.Evaluate(m_fill.fillAmount);
            yield return null;
            currentTime += Time.deltaTime;
        }
    }
    public void PerderVida()
    {
        StopAllCoroutines();
        m_lastHp = m_currentHp;
        m_currentHp = m_SOPlayer.hp;
        StartCoroutine(ActualitzarVida());
    }
}
