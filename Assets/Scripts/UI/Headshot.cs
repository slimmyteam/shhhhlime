using System.Collections;
using UnityEngine;

public class Headshot : MonoBehaviour
{
    [Header("Referencias a objetos")]
    [SerializeField]
    private GameObject m_head;
    [SerializeField]
    GameObject canvasHeadshot;

    [Header("Headshot")]
    [SerializeField]
    private int timeActive = 5;
    [SerializeField]
    private float rotationSpeed = 50f;

    [Header("Sfx")]
    private AudioSource m_AudioHeadshot;


    private void Awake()
    {
        m_head.GetComponent<HurtBox>().OnHeadshot += ReciboHeadshot;
        m_AudioHeadshot = GetComponent<AudioSource>();
    }

    private void ReciboHeadshot()
    {
        StartCoroutine(MostrarHeadshot());
    }

    private void Update()
    {
        canvasHeadshot.transform.Rotate(Vector3.up * rotationSpeed * Time.deltaTime);
    }

    private IEnumerator MostrarHeadshot()
    {
        m_AudioHeadshot.Play();
        canvasHeadshot.SetActive(true);
        yield return new WaitForSeconds(timeActive);
        canvasHeadshot.SetActive(false);
    }

    private void OnDestroy()
    {
        m_head.GetComponent<HurtBox>().OnHeadshot -= ReciboHeadshot;
    }
}
