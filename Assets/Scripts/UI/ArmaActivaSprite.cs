using UnityEngine;
using UnityEngine.UI;

public class ArmaActivaSprite : MonoBehaviour
{
    [Header("Materiales de sprites de armas")]
    [SerializeField]
    private Texture m_GunSprite;
    [SerializeField]
    private Texture m_KnifeSprite;

    [Header("Referencias")]
    [SerializeField]
    private RawImage m_ArmaSprite;

    [Header("Variables")]
    [SerializeField]
    private float m_AlphaSpriteCooldown;
    [SerializeField]
    private float m_FadeAlphaDuration;

    private void Start()
    {
        m_ArmaSprite.texture = m_GunSprite;
    }

    public void CambiarSpriteArma(int arma)
    {
        switch (arma)
        {
            case 0:
                m_ArmaSprite.texture = m_GunSprite;
                break;
            case 1:
                m_ArmaSprite.texture = m_KnifeSprite;
                break;
        }
    }

    public void CooldownSprite()
    {
        m_ArmaSprite.CrossFadeAlpha(m_AlphaSpriteCooldown, m_FadeAlphaDuration, false);
    }

    public void FinCooldown()
    {
        m_ArmaSprite.CrossFadeAlpha(1, m_FadeAlphaDuration, false);
    }


}
