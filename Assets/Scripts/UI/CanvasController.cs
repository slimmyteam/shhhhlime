using System.Collections;
using TMPro;
using UnityEngine;

public class CanvasController : MonoBehaviour
{
    [Header("Hackeo")]
    [SerializeField]
    private GameObject m_TextoHackeando;

    [Header("Slimmes")]
    [SerializeField]
    private GameObject m_SlimeTrophy;
    [SerializeField]
    private TextMeshProUGUI m_CantidadSlimmesConseguidos;
    private int slimesConseguidos = 0;
    private int totalSlimes = 4;

    [Header("Backstab")]
    [SerializeField]
    private GameObject m_TextoBackstab;
    [SerializeField]
    private float m_TiempoBackstabShort = 0.2f;
    [SerializeField]
    private float m_TiempoBackstabLong = 3;

    [Header("Sfx")]
    private AudioSource m_AudioBackstab;

    [Header("Armas")]
    [SerializeField]
    private GameObject m_ArmasSprite;

    private void Awake()
    {
        m_AudioBackstab = GetComponent<AudioSource>();
        m_ArmasSprite.SetActive(true);

        totalSlimes = 4;
        slimesConseguidos = 0;
        m_CantidadSlimmesConseguidos.SetText(slimesConseguidos + "/" + totalSlimes);
        m_CantidadSlimmesConseguidos.enabled = true;
        m_SlimeTrophy.SetActive(true);
    }

    public void ActivarTextoHackeando()
    {
        m_TextoHackeando.SetActive(!m_TextoHackeando.activeInHierarchy);
    }

    public void ActivarTextoBackstab()
    {
        StartCoroutine(ActivamosBackstab());
    }

    private IEnumerator ActivamosBackstab()
    {
        m_AudioBackstab.Play();
        m_TextoBackstab.SetActive(true);
        yield return new WaitForSeconds(m_TiempoBackstabShort);
        m_TextoBackstab.SetActive(false);
        yield return new WaitForSeconds(m_TiempoBackstabShort);
        m_TextoBackstab.SetActive(true);
        yield return new WaitForSeconds(m_TiempoBackstabLong);
        m_TextoBackstab.SetActive(false);
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    public void DesactivarElementosUI()
    {
        m_ArmasSprite?.SetActive(false);
        m_SlimeTrophy.SetActive(false);
        m_CantidadSlimmesConseguidos.enabled = false;
    }

    public void RestarContadorSlimmes(int contadorSlimes)
    {
        slimesConseguidos += contadorSlimes;
        m_CantidadSlimmesConseguidos.SetText(slimesConseguidos + "/" + totalSlimes);
    }
}
