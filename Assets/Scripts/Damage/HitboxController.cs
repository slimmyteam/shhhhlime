using UnityEngine;

public class HitboxController : MonoBehaviour
{
    [Header("Stats")]
    private int m_Dmg;
    public int Dmg { get { return m_Dmg; } set { m_Dmg = value; } }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag != "Player" && other.TryGetComponent(out IDamageable target))
            target.Damage(m_Dmg);
    }
}
