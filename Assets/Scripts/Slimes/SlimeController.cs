using UnityEngine;

public class SlimeController : MonoBehaviour
{
    [SerializeField]
    private GameEvent restoSlime;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            restoSlime.Raise();
            this.gameObject.SetActive(false);
        }
    }
}
