using UnityEngine;

public class GlobalSlimeController : MonoBehaviour
{
    [Header("Slimes")]
    [SerializeField]
    private GameObject[] listaSlimes;
    [SerializeField]
    private GameObject SlimeGoldo;
    private int contSlimes;

    [Header("Game Event")]
    [SerializeField]
    private GEInt mostrarContadorSlimes;

    private void Awake()
    {
        foreach (GameObject obj in listaSlimes)
        {
            obj.SetActive(true);
        }
        SlimeGoldo.SetActive(false);
        contSlimes = 4;
    }

    private void SpawnearSlimeGoldo()
    {
        SlimeGoldo.SetActive(true);
    }

    public void restarCont()
    {
        contSlimes--;
        mostrarContadorSlimes.Raise(1);
        if (contSlimes <= 0)
            SpawnearSlimeGoldo();
    }
}
