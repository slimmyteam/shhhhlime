using UnityEngine;

public class Sound
{
    public readonly float soundRange;
    public readonly Transform soundPosition;
    public readonly SoundType.type soundType;

    public Sound(float soundRange, Transform soundPosition, SoundType.type soundType)
    {
        this.soundRange = soundRange;
        this.soundPosition = soundPosition;
        this.soundType = soundType;
    }

    public void MakeSound()
    {
        Collider[] collisions = Physics.OverlapSphere(this.soundPosition.position, this.soundRange); //no tiene collider el enemy xd
        for (int i = 0; i < collisions.Length; i++)
        {
            if (collisions[i].TryGetComponent(out IReactToSound target))
            {
                target.ReactToSound(this);
            }
        }
    }
}
