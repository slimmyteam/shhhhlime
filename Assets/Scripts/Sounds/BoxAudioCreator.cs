using UnityEngine;

public class BoxAudioCreator : MonoBehaviour
{
    private Sound sonidoCaja;

    private void Start()
    {
        sonidoCaja = new Sound(10f, this.transform, SoundType.type.Interesting);
    }
    void Update()
    {
        if (this.gameObject.GetComponent<Rigidbody>().velocity != Vector3.zero)
        {
            if (!this.gameObject.GetComponent<AudioSource>().isPlaying)
                this.gameObject.GetComponent<AudioSource>().Play();
            sonidoCaja.MakeSound();
        }

    }
}
