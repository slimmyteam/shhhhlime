using UnityEngine;

[CreateAssetMenu]
public class SOPlayer : ScriptableObject
{
    public int maxHP;
    public int hp;
}
