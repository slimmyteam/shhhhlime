using System;
using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.VFX;

public class PlayerController : MonoBehaviour, IDamageable
{
    [Header("Componentes")]
    [SerializeField]
    private Rigidbody m_Rigidbody;
    [SerializeField]
    private Animator m_Animator;
    [SerializeField]
    private BoxCollider m_BoxCollider;

    [Header("Inputs")]
    [SerializeField]
    private InputActionAsset m_InputActionAsset;
    private InputActionAsset m_Input;
    private InputAction m_MovementAction;
    private InputAction m_MovementClimbingLadder;

    [Header("Referencias a objetos")]
    [SerializeField]
    private Camera m_Camera;
    [SerializeField]
    private GameObject m_KnifeObject;
    [SerializeField]
    private GameObject m_Pistola;
    [SerializeField]
    private enum ObjetosInteractuables { NONE, ESCALERA };
    [SerializeField]
    private ObjetosInteractuables m_ObjetoInteractuable;
    private RaycastHit hitPared;

    [Header("State Machine")]
    [SerializeField]
    private SwitchMachineStates m_CurrentState;
    [SerializeField]
    private SwitchMachineStates m_MemoryState;
    private enum SwitchMachineStates { NONE, IDLE, WALK, SHOOT, KNIFE, JUMP, WALLJUMP, INTERACT, GANCHO, PUPA, CLIMB, DEAD };

    [Header("Stats")]
    [SerializeField]
    private SOPlayer m_SOPlayer;

    [Header("Movement")]
    [SerializeField]
    private float m_MovementSpeed = 3f;
    Vector3 m_Movement;
    [SerializeField]
    private Vector3 m_ForwardDirection;
    [SerializeField]
    private bool m_Deslizandose;

    [Header("Jump")]
    [SerializeField]
    private float m_JumpMaxHeight;
    [SerializeField]
    private float m_CompletionJumpTime;
    [SerializeField]
    private float m_MultiplicadorVelocidadCaida;
    [SerializeField]
    private LayerMask m_SaltableMask;
    [SerializeField]
    private float m_RaycastFloorDistance;
    [SerializeField]
    private float m_Gravity;
    [SerializeField]
    private float m_InitialJumpVelocity;
    private float initialHeight;

    [Header("Wall Jump")]
    [SerializeField]
    private float m_multiplicadorFuerzaSalto = 5f;

    [Header("Climb")]
    [SerializeField]
    Transform PosicionEscalar;

    [Header("Damage")]
    [SerializeField]
    private int m_Dmg;
    [SerializeField]
    private int m_KnifeDmg;
    [SerializeField]
    private float m_PushForce; // Fuerza con la que se empuja a un enemigo
    [SerializeField]
    private float m_CooldownTime;

    [Header("Disparos")]
    [SerializeField]
    private Vector3 m_MousePosition;
    [SerializeField]
    private LayerMask m_ShootMask;
    [SerializeField]
    private float m_ShootDistance;
    [SerializeField]
    private TrailRenderer m_TrailRenderer;
    [SerializeField]
    private VisualEffect m_BulletHitEffect;
    [SerializeField]
    private VisualEffect m_BulletHitEnemyEffect;

    [Header("Gancho")]
    [SerializeField]
    private LayerMask m_GanchoMask;
    [SerializeField]
    private float m_GanchoDistance;
    [SerializeField]
    private float m_GanchoSpeed;
    [SerializeField]
    private Vector3 m_DestinoGancho;
    [SerializeField]
    private float m_DistanciaParaEstarEnDestino; //Distancia para que se considere que el personaje ha llegado al destino del gancho

    [Header("Knife / Backstab")]
    [SerializeField]
    private bool m_KnifeActive;

    [Header("Hackear")]
    [SerializeField]
    private float m_TiempoHackeo;
    private Coroutine m_HackeandoCoroutine;
    [SerializeField]
    private GameObject m_CamaraHackeable;

    [Header("Variables de control")]
    [SerializeField]
    private bool m_Cooldown;
    private float currentTime;
    [SerializeField]
    private bool m_Invulnerable;
    [SerializeField]
    private bool m_Agachado = false;
    [SerializeField]
    private bool m_Jumping = false;
    [SerializeField]
    private bool hitParedIsSet = false;

    //[Header("Delegados")]
    public event Action<int> OnDamage;

    [Header("BodyParts")]
    [SerializeField]
    private Rigidbody[] m_BodyParts;

    [Header("Animations")]
    [SerializeField]
    private float m_TransitionDuration;

    [Header("Eventos")]
    [SerializeField]
    private GameEvent m_PlayerTakesDamage;
    [SerializeField]
    private GameEvent m_PlayerHaMuerto;
    [SerializeField]
    private GEInt m_CambiarArma;
    [SerializeField]
    private GameEvent m_PlayerCooldown;
    [SerializeField]
    private GameEvent m_FinCooldown;
    [SerializeField]
    private GameEvent m_Hackeando;

    [Header("Sounds")]
    [SerializeField]
    private AudioSource m_AudioSource;
    [SerializeField]
    private AudioClip[] m_AudioClips;

    private void Awake()
    {
        //Componentes
        m_Rigidbody = GetComponent<Rigidbody>();
        m_BoxCollider = GetComponent<BoxCollider>();

        //Inputs ("Default")
        m_Input = Instantiate(m_InputActionAsset);
        m_MovementAction = m_Input.FindActionMap("Default").FindAction("Movement");
        m_Input.FindActionMap("Default").FindAction("Crouch").performed += Agacharse;
        m_Input.FindActionMap("Default").FindAction("Jump").started += Saltar;
        m_Input.FindActionMap("Default").FindAction("Jump").canceled += DejarSaltar;
        m_Input.FindActionMap("Default").FindAction("Attack").performed += Attack;
        m_Input.FindActionMap("Default").FindAction("ChangeWeapon").performed += ChangeWeapon;
        m_Input.FindActionMap("Default").FindAction("Interact").performed += CambiarEstadoAInteract;
        m_Input.FindActionMap("Default").FindAction("Gancho").performed += Gancho;
        m_Input.FindActionMap("Default").FindAction("Hack").performed += Hack;
        m_Input.FindActionMap("Default").FindAction("Hack").canceled += CancelHack;
        m_Input.FindActionMap("Default").Enable();

        //Inputs ("MovementLadder")
        m_MovementClimbingLadder = m_Input.FindActionMap("LadderClimbing").FindAction("MovementLadder");

        //Inputs ("CameraHacking")
        m_Input.FindActionMap("CameraHacking").FindAction("ExitCamera").performed += ExitCamera;

        //Movimiento
        m_ForwardDirection = transform.forward;
        m_Movement = Vector3.zero;
        m_Camera.GetComponent<CameraController>().NewForwardDirection += RecalculateForward;

        m_Invulnerable = false;
        m_KnifeObject.SetActive(false);
        m_ObjetoInteractuable = ObjetosInteractuables.NONE;
        m_SOPlayer.hp = m_SOPlayer.maxHP;

        foreach (Rigidbody part in m_BodyParts)
        {
            part.gameObject.GetComponent<HurtBox>().OnDamage += Damage;
            part.isKinematic = true;
        }
    }

    void Start()
    {
        ChangeState(SwitchMachineStates.IDLE);
    }

    void Update()
    {
        UpdateState();
    }

    //----------------------------------[ ATAQUES }----------------------------------//

    private void Attack(InputAction.CallbackContext context)
    {
        if (m_KnifeActive)
        {
            ChangeState(SwitchMachineStates.KNIFE);
            return;
        }
        else
        {
            ChangeState(SwitchMachineStates.SHOOT);
        }
    }

    private void ChangeWeapon(InputAction.CallbackContext context)
    {
        m_KnifeActive = !m_KnifeActive;
        m_KnifeObject.SetActive(m_KnifeActive);
        m_Pistola.SetActive(!m_KnifeActive);
        m_CambiarArma.Raise(m_KnifeActive ? 1 : 0);
    }

    private void KnifeAttack()
    {
        float dotVector = 0;
        RaycastHit hit;
        if (Physics.Raycast((transform.position + new Vector3(0, 1, 0)), transform.forward, out hit, 3, m_ShootMask))
        {
            dotVector = Vector3.Dot(hit.transform.forward, transform.forward);
            Debug.DrawLine((transform.position + new Vector3(0, 1, 0)), hit.point, Color.magenta, 2f);
        }
        if (dotVector >= 0.9f)
        {
            m_KnifeObject.GetComponent<HitboxController>().Dmg = m_KnifeDmg * 1000;
            m_Animator.CrossFade("Stabbing", m_TransitionDuration);

        }
        else
        {
            m_KnifeObject.GetComponent<HitboxController>().Dmg = m_KnifeDmg;
            m_Animator.CrossFade("Stabbing", m_TransitionDuration);
        }

        StartCoroutine(Cooldown());
    }

    public void Shoot()
    {

        m_MousePosition = m_Camera.ScreenToWorldPoint(new Vector3(Mouse.current.position.value.x, Mouse.current.position.value.y, m_Camera.nearClipPlane));
        Vector3 direccionRaycast = (m_MousePosition - m_Camera.transform.position).normalized;
        TrailRenderer trail = Instantiate(m_TrailRenderer, m_Pistola.transform.position, Quaternion.identity);
        trail.AddPosition(m_Pistola.transform.position);

        if (Physics.Raycast(m_Camera.transform.position, direccionRaycast, out RaycastHit hit)) //Se comprueba a donde apuntas desde la camara
        {
            //Debug.Log($"He tocat {hit.collider.gameObject} a la posicio {hit.point} amb normal {hit.normal}");
            Vector3 direccionDisparo = (hit.point - m_Pistola.transform.position).normalized;
            Debug.DrawLine(m_Camera.transform.position, hit.point, Color.blue, 2f);
            transform.forward = new Vector3(direccionDisparo.x, 0, direccionDisparo.z);

            if (Physics.Raycast(m_Pistola.transform.position, direccionDisparo, out RaycastHit hitDisparo, m_ShootDistance, m_ShootMask)) //Se dispara desde el player hasta el objetivo
            {
                trail.transform.position = hitDisparo.point;
                //Debug.Log($"He tocat {hitDisparo.collider.gameObject} a la posicio {hitDisparo.point} amb normal {hitDisparo.normal}");
                Debug.DrawLine(m_Pistola.transform.position, hitDisparo.point, Color.green, 2f);
                if (hitDisparo.collider.TryGetComponent(out IDamageable target))
                {
                    target.Damage(m_Dmg);
                    VisualEffect visualEffect = Instantiate(m_BulletHitEnemyEffect);
                    visualEffect.transform.position = hitDisparo.point;
                    visualEffect.transform.forward = hitDisparo.normal;
                    visualEffect.Play();
                }
                else
                {
                    VisualEffect visualEffect = Instantiate(m_BulletHitEffect);
                    visualEffect.transform.position = hitDisparo.point;
                    visualEffect.transform.forward = hitDisparo.normal;
                    visualEffect.Play();
                }
                if (hitDisparo.collider.TryGetComponent(out IPushable pushable))
                    pushable.Push(transform.forward, m_PushForce);
            }
            else
            {
                //Disparo más lejos de la distancia de disparo: falla el disparo, pero se dibuja el trail

                // Vector3 puntoFinaldelBulletTrail = (transform.position + new Vector3(0, 1, 0)) + transform.forward * 20;
                Vector3 puntoFinaldelBulletTrail = hit.point;
                trail.transform.position = puntoFinaldelBulletTrail;
            }




        }
        else
        {
            Vector3 puntoFinaldelBulletTrail = (transform.position + new Vector3(0, 1, 0)) + transform.forward * 20;
            trail.transform.position = puntoFinaldelBulletTrail;
        }
        m_AudioSource.clip = m_AudioClips[0];
        m_AudioSource.loop = false;
        m_AudioSource.Play();
        Sound sonidoDisparo = new Sound(20f, m_Pistola.transform, SoundType.type.Interesting);
        sonidoDisparo.MakeSound();

        StartCoroutine(Cooldown());
    }


    //----------------------------------[ HABILIDADES }----------------------------------//    

    //---------------- GANCHO
    private void Gancho(InputAction.CallbackContext context)
    {
        m_MousePosition = m_Camera.ScreenToWorldPoint(new Vector3(Mouse.current.position.value.x, Mouse.current.position.value.y, m_Camera.nearClipPlane));
        Vector3 direccionRaycast = (m_MousePosition - m_Camera.transform.position).normalized;
        if (Physics.Raycast(m_Camera.transform.position, direccionRaycast, out RaycastHit hit))
        {
            //Debug.Log($"Lanzo el gancho a {hit.collider.gameObject} a la posicion {hit.point} con normal {hit.normal}");
            Debug.DrawLine(m_Camera.transform.position, hit.point, Color.red, 2f);

            Vector3 direccion = (hit.point - transform.position).normalized;

            if (Physics.Raycast(transform.position, direccion, out RaycastHit hitGancho, m_GanchoDistance, m_GanchoMask))
            {
                Debug.DrawLine(transform.position, hitGancho.point, Color.magenta, 2f);


                m_DestinoGancho = hitGancho.point;
                ChangeState(SwitchMachineStates.GANCHO);

            }

        }
    }

    //-------------- HACKEAR
    private void Hack(InputAction.CallbackContext context)
    {
        m_MousePosition = m_Camera.ScreenToWorldPoint(new Vector3(Mouse.current.position.value.x, Mouse.current.position.value.y, m_Camera.nearClipPlane));
        Vector3 direccionRaycast = (m_MousePosition - m_Camera.transform.position).normalized;
        if (Physics.Raycast(m_Camera.transform.position, direccionRaycast, out RaycastHit hit))
        {
            Debug.DrawLine(m_Camera.transform.position, hit.point, Color.red, 2f);

            if (hit.collider.gameObject.CompareTag("CamaraHackeable"))
            {
                m_CamaraHackeable = hit.collider.gameObject;
                m_HackeandoCoroutine = StartCoroutine(Hackeando());
            }
        }
    }

    private void CancelHack(InputAction.CallbackContext context)
    {
        if (m_CamaraHackeable != null)
            m_Hackeando.Raise();
        if (m_HackeandoCoroutine != null)
            StopCoroutine(m_HackeandoCoroutine);
    }

    private IEnumerator Hackeando()
    {
        m_Hackeando.Raise();
        yield return new WaitForSeconds(m_TiempoHackeo);
        CambiarACamaraHackeable();
    }

    private void CambiarACamaraHackeable()
    {
        m_CamaraHackeable.GetComponent<CamaraHackeableController>().ActivarDesactivarCamara();
        m_Camera.gameObject.SetActive(false);
        m_Input.FindActionMap("Default").Disable();
        m_Input.FindActionMap("CameraHacking").Enable();
    }

    private void ExitCamera(InputAction.CallbackContext context)
    {
        m_Input.FindActionMap("CameraHacking").Disable();
        m_Input.FindActionMap("Default").Enable();
        m_CamaraHackeable.GetComponent<CamaraHackeableController>().ActivarDesactivarCamara();
        m_Camera.gameObject.SetActive(true);
        m_CamaraHackeable = null;
    }


    //----------------------------------[ MÁQUINA DE ESTADOS }----------------------------------//
    private void ChangeState(SwitchMachineStates newState)
    {/*
        if (newState == m_CurrentState)
            return;  */
        if (m_CurrentState == SwitchMachineStates.DEAD)
            return;
        if (newState == SwitchMachineStates.PUPA)
        {
            if (m_CurrentState == SwitchMachineStates.SHOOT || m_CurrentState == SwitchMachineStates.PUPA)
                m_MemoryState = SwitchMachineStates.IDLE;
            else m_MemoryState = m_CurrentState;
        }
        ExitState();
        InitState(newState);
        // Debug.Log("STATE: "+newState);
    }


    private void InitState(SwitchMachineStates currentState)
    {
        m_CurrentState = currentState;
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                RecalculateForward();
                if (m_Agachado)
                    m_Animator.CrossFade("Crouch Idle", m_TransitionDuration);
                else
                    m_Animator.CrossFade("Idle", m_TransitionDuration);
                m_Rigidbody.velocity = Vector3.zero;
                break;
            case SwitchMachineStates.WALK:

                if (!m_Agachado)
                {
                    m_AudioSource.clip = m_AudioClips[1];
                    m_AudioSource.loop = true;
                    m_AudioSource.Play();
                    m_Animator.CrossFade("Running", m_TransitionDuration);
                }
                else
                    m_Animator.CrossFade("Crouch Walk Forward", m_TransitionDuration);
                break;
            case SwitchMachineStates.SHOOT:
                if (!m_Cooldown)
                {
                    m_Animator.CrossFade("Shooting", m_TransitionDuration);
                    //Shoot();
                }
                break;
            case SwitchMachineStates.JUMP:
                m_Animator.CrossFade("Falling Idle", m_TransitionDuration);
                m_Movement.y += m_InitialJumpVelocity;
                currentTime = 0f;
                initialHeight = transform.position.y;
                break;
            case SwitchMachineStates.WALLJUMP:
                gameObject.transform.forward = hitPared.normal;
                m_Movement.y += m_InitialJumpVelocity;
                m_Rigidbody.AddForce(hitPared.normal * m_multiplicadorFuerzaSalto, ForceMode.Impulse);
                initialHeight = transform.position.y;
                ChangeState(SwitchMachineStates.JUMP);
                break;
            case SwitchMachineStates.KNIFE:
                if (!m_Cooldown)
                    KnifeAttack();
                break;
            case SwitchMachineStates.PUPA:
                m_Animator.CrossFade("Pain Gesture", m_TransitionDuration);
                break;
            case SwitchMachineStates.CLIMB:
                m_Animator.Play("Climbing Ladder");
                //.CrossFade para que las animaciones tengan transicion (se vean mas naturales)
                transform.position = PosicionEscalar.transform.position;
                transform.forward = PosicionEscalar.transform.right;
                m_Input.FindActionMap("Default").Disable();
                m_Input.FindActionMap("LadderClimbing").Enable();
                m_Rigidbody.useGravity = false;
                break;
            case SwitchMachineStates.DEAD:
                m_PlayerHaMuerto.Raise();
                m_Animator.enabled = false;
                StopAllCoroutines();
                Desuscribe();
                break;
            case SwitchMachineStates.GANCHO:
                m_Animator.CrossFade("Falling Idle", m_TransitionDuration);
                Vector3 direccion = (m_DestinoGancho - transform.position).normalized;
                m_Rigidbody.velocity = direccion * m_GanchoSpeed;
                transform.forward = direccion;
                break;
        }
    }

    private void UpdateState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                //MouseRotation();
                if (m_MovementAction.ReadValue<Vector3>().x != 0 || m_MovementAction.ReadValue<Vector3>().y != 0 || m_MovementAction.ReadValue<Vector3>().z != 0)
                    ChangeState(SwitchMachineStates.WALK);
                break;

            case SwitchMachineStates.WALK:
                m_Movement = Vector3.zero;

                if (m_MovementAction.ReadValue<Vector3>().z != 0)
                {
                    RecalculateForward();
                    m_Movement += m_MovementAction.ReadValue<Vector3>().z * m_ForwardDirection;
                }

                if (m_MovementAction.ReadValue<Vector3>().x > 0)
                {
                    m_Movement += m_Camera.transform.right;
                }
                else if (m_MovementAction.ReadValue<Vector3>().x < 0)
                {
                    m_Movement -= m_Camera.transform.right;
                }

                //Esto es para evitar el error de Unity 'Look rotation vector is zero'
                if (m_Movement != Vector3.zero)
                    transform.forward = m_Movement;

                if (!m_Deslizandose)
                    m_Rigidbody.velocity = m_Movement.normalized * m_MovementSpeed + Vector3.up * m_Rigidbody.velocity.y;
                else
                    m_Rigidbody.AddForce(m_Movement.normalized * m_MovementSpeed + Vector3.up * m_Rigidbody.velocity.y);

                if (m_Rigidbody.velocity == Vector3.zero) //mirar esto por si caes de un sitio alto 
                    ChangeState(SwitchMachineStates.IDLE);

                if (m_MovementAction.ReadValue<Vector3>().x == 0 && m_MovementAction.ReadValue<Vector3>().y == 0 && m_MovementAction.ReadValue<Vector3>().z == 0)
                    ChangeState(SwitchMachineStates.IDLE);
                else if (!m_Agachado)
                {
                    Sound sonidoWalk = new Sound(5f, this.transform, SoundType.type.Interesting);
                    sonidoWalk.MakeSound();
                }

                break;
            case SwitchMachineStates.KNIFE:
                break;
            case SwitchMachineStates.SHOOT:
                break;
            case SwitchMachineStates.GANCHO:
                if (Vector3.Distance(m_DestinoGancho, transform.position) <= m_DistanciaParaEstarEnDestino)
                {
                    ChangeState(SwitchMachineStates.IDLE);
                }
                break;
            case SwitchMachineStates.JUMP:
                GravityController();
                //if (isGrounded()) ChangeState(SwitchMachineStates.IDLE);
                break;
            case SwitchMachineStates.CLIMB:
                m_Movement = Vector3.zero;

                if (m_MovementClimbingLadder.ReadValue<Vector3>().y > 0)
                {
                    m_Movement += transform.up;
                }
                else if (m_MovementClimbingLadder.ReadValue<Vector3>().y < 0)
                {
                    m_Movement -= transform.up;
                }

                m_Rigidbody.velocity = m_Movement.normalized * m_MovementSpeed;

                break;

            default:
                if (m_Cooldown)
                {
                    ChangeState(SwitchMachineStates.IDLE);
                }
                break;
        }
    }

    private void ExitState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.JUMP:
                //m_Rigidbody.velocity = new Vector3(m_Rigidbody.velocity.x, 0, m_Rigidbody.velocity.z);
                //StopCoroutine(GravityController()); <-- hay que guardrarse la corrutina primero!!
                m_Movement = new Vector3(m_Movement.x, 0, m_Movement.z);
                StopAllCoroutines();
                break;
            case SwitchMachineStates.WALLJUMP:
                hitParedIsSet = false;
                break;
            case SwitchMachineStates.CLIMB:
                m_Input.FindActionMap("Default").Enable();
                m_Input.FindActionMap("LadderClimbing").Disable();
                m_Rigidbody.useGravity = true;
                break;
            case SwitchMachineStates.PUPA:
                m_Invulnerable = false;
                break;
            case SwitchMachineStates.GANCHO:
                m_DestinoGancho = Vector3.zero;
                transform.forward = new Vector3(transform.forward.x, 0, transform.forward.z);
                break;
            case SwitchMachineStates.WALK:
                m_AudioSource.loop = false;
                break;
            default:
                break;
        }
    }


    //-------------------------------[ MOVIMIENTO ]-------------------------------//
    private void RecalculateForward()
    {
        m_ForwardDirection = Vector3.ProjectOnPlane(m_Camera.transform.forward, transform.up);
    }

    private void Agacharse(InputAction.CallbackContext actionContext)
    {
        if (IsGrounded())
        {
            m_Agachado = !m_Agachado;
            if (m_Agachado)
            {
                m_BoxCollider.size = new Vector3(m_BoxCollider.size.x, m_BoxCollider.size.y / 2, m_BoxCollider.size.z);
                m_BoxCollider.center = new Vector3(m_BoxCollider.center.x, m_BoxCollider.center.y / 2, m_BoxCollider.center.z);
                m_MovementSpeed /= 2;
            }
            else
            {
                m_BoxCollider.size = new Vector3(m_BoxCollider.size.x, m_BoxCollider.size.y * 2, m_BoxCollider.size.z);
                m_BoxCollider.center = new Vector3(m_BoxCollider.center.x, m_BoxCollider.center.y * 2, m_BoxCollider.center.z);
                m_MovementSpeed *= 2;
            }
            ChangeState(m_CurrentState);
        }
    }

    //-------------------------------[ CORUTINAS ]-------------------------------//

    private IEnumerator Cooldown()
    {
        m_Cooldown = true;
        m_PlayerCooldown.Raise();
        yield return new WaitForSeconds(m_CooldownTime);
        m_Cooldown = false;
        m_FinCooldown.Raise();
    }

    //-------------------------------[ GRAVITY ]-------------------------------//

    private void Saltar(InputAction.CallbackContext actionContext)
    {
        print(IsGrounded());
        if (IsGrounded())
        {
            m_Jumping = true;
            ChangeState(SwitchMachineStates.JUMP);
        }

        if (m_CurrentState == SwitchMachineStates.WALLJUMP) return;

        if (hitParedIsSet) ChangeState(SwitchMachineStates.WALLJUMP);
    }

    private void DejarSaltar(InputAction.CallbackContext actionContext)
    {
        m_Jumping = false;
    }

    private bool IsGrounded()
    {
        RaycastHit hit;
        if (Physics.Raycast(this.transform.position, -transform.up, out hit, m_RaycastFloorDistance, m_SaltableMask))
        {
            Debug.DrawLine(this.transform.position, hit.point, Color.yellow, 2f);
            return true;
        }
        else return false;
    }

    private void GravityController()
    {

        if (currentTime > m_CompletionJumpTime * m_InitialJumpVelocity)
            ChangeState(SwitchMachineStates.IDLE);

        bool falling = false;
        if (currentTime >= m_CompletionJumpTime / 2 || !m_Jumping || (transform.position.y - initialHeight) >= m_JumpMaxHeight)
            falling = true;
        if (falling)
        {
            Caerse();
            if (IsGrounded())
            {
                ChangeState(SwitchMachineStates.IDLE);
            }
        }
        else
        {
            float previousYVelocity = m_Movement.y;
            float newYVelocity = m_Movement.y + (m_Gravity * Time.deltaTime);
            m_Movement.y = (previousYVelocity + newYVelocity) / 2;
        }

        m_Rigidbody.velocity = new Vector3(m_Rigidbody.velocity.x, m_Movement.y, m_Rigidbody.velocity.z);
        currentTime += Time.deltaTime;
    }

    private void Caerse()
    {
        float previousYVelocity = m_Movement.y;
        float newYVelocity = m_Movement.y + (m_Gravity * m_MultiplicadorVelocidadCaida * Time.deltaTime);
        m_Movement.y = (previousYVelocity + newYVelocity) / 2;
    }

    //-------------------------------[ DAMAGE ]-------------------------------//

    public void Damage(int amount)
    {
        if (!m_Invulnerable)
        {
            print(this.name + " HA PERDIDO " + amount + " DE VIDA");
            m_SOPlayer.hp -= amount;
            m_PlayerTakesDamage.Raise();
            if (m_SOPlayer.hp <= 0 && m_CurrentState != SwitchMachineStates.DEAD)
            {
                ChangeState(SwitchMachineStates.DEAD);
            }
            if (m_CurrentState != SwitchMachineStates.CLIMB)
            {
                m_Invulnerable = true;
                ChangeState(SwitchMachineStates.PUPA);
            }
        }
    }

    //-------------------------------[ CAMBIAR DE ESTADOS ]-------------------------------//

    public void ReturnToMemoryState()
    {
        ChangeState(m_MemoryState);
    }

    public void BackToIdle()
    {
        ChangeState(SwitchMachineStates.IDLE);
    }

    private void CambiarEstadoAInteract(InputAction.CallbackContext actionContext)
    {
        if (m_ObjetoInteractuable != ObjetosInteractuables.NONE)
        {
            ChangeState(SwitchMachineStates.INTERACT);
            switch (m_ObjetoInteractuable)
            {
                case ObjetosInteractuables.ESCALERA:
                    ChangeState(SwitchMachineStates.CLIMB);
                    break;
            }
        }
    }

    //-------------------------------[ COLISIONES ]-------------------------------//
    private void OnTriggerExit(Collider other)
    {
        m_ObjetoInteractuable = ObjetosInteractuables.NONE;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Trigger") && (m_CurrentState == SwitchMachineStates.WALK || m_CurrentState == SwitchMachineStates.IDLE))
        {
            switch (other.tag)
            {
                case "Escalera":
                    if (m_ObjetoInteractuable == ObjetosInteractuables.ESCALERA) return;

                    PosicionEscalar = other.GetComponent<Escalera>().PosicionEscalar;

                    m_ObjetoInteractuable = ObjetosInteractuables.ESCALERA;

                    break;
            }
        }
        else if (other.gameObject.layer == LayerMask.NameToLayer("Trigger") && m_CurrentState == SwitchMachineStates.CLIMB)
        {
            PosicionEscalar = other.GetComponent<Escalera>().PosicionEscalar;

            transform.position = PosicionEscalar.transform.position;
            ChangeState(SwitchMachineStates.IDLE);
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Slippery"))
        {
            m_Deslizandose = false;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Slippery"))
        {
            m_Deslizandose = true;
        }

        if (collision.gameObject.layer == LayerMask.NameToLayer("Scenario") && collision.gameObject.tag == "Wall")
        {
            print("He chocado con escenario y el tag del objeto es: " + collision.gameObject.tag);
            if (IsGrounded())
            {
                return;
            }

            if (Physics.Raycast(transform.position, transform.forward, out hitPared, 3, m_ShootMask)) //Cogemos la normal de la pared con un raycast
            {
                hitParedIsSet = true;
                if (hitPared.normal.y < 0.1f)
                {
                    Debug.DrawRay(hitPared.point, hitPared.normal, Color.red, 2f);
                }
            }
        }
    }

    //-------------------------------[ DESUSCRIBIRSE ]-------------------------------//

    private void Desuscribe()
    {
        m_Input.FindActionMap("Default").FindAction("Crouch").performed -= Agacharse;
        m_Input.FindActionMap("Default").FindAction("Jump").started -= Saltar;
        m_Input.FindActionMap("Default").FindAction("Jump").canceled -= DejarSaltar;
        m_Input.FindActionMap("Default").FindAction("Attack").performed -= Attack;
        m_Input.FindActionMap("Default").FindAction("ChangeWeapon").performed -= ChangeWeapon;
        m_Input.FindActionMap("Default").Disable();
        foreach (Rigidbody part in m_BodyParts)
        {
            part.gameObject.GetComponent<HurtBox>().OnDamage -= Damage;
            part.isKinematic = false;
        }
        m_Animator.enabled = false;
    }
}
