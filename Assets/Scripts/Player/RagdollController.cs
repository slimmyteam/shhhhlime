using UnityEngine;
using UnityEngine.VFX;

public class RagdollController : MonoBehaviour
{
    [Header("Eventos")]
    [SerializeField]
    private GameEvent m_VolverAIdle;
    [SerializeField]
    private GameEvent volverMemoryState;
    [SerializeField]
    private VisualEffect m_MuzzleFlash;
    [SerializeField]
    private GameEvent m_DispararArma;

    public void TerminarAnimacionYVolverAIdle()
    {
        m_VolverAIdle.Raise();
    }

    public void terminarAnimacionYVolverAMemoryState()
    {
        volverMemoryState.Raise();
    }

    public void activarMuzzleFlash()
    {
        m_MuzzleFlash.Play();
    }

    public void activarDispararArma()
    {
        m_DispararArma.Raise();
    }
}
