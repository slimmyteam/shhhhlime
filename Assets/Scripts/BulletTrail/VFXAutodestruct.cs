using UnityEngine;

public class VFXAutodestruct : MonoBehaviour
{
    [SerializeField]
    float tiempoAutodestruct;

    void Start()
    {
        Destroy(gameObject, tiempoAutodestruct);
    }
}
