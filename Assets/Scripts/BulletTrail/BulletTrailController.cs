using System.Collections;
using UnityEngine;

public class BulletTrailController : MonoBehaviour
{
    void Start()
    {
        StartCoroutine(fadeOut());
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    private IEnumerator fadeOut()
    {
        TrailRenderer trail = gameObject.GetComponent<TrailRenderer>();
        float currentTime = 0f;
        float alpha = 1;
        while (currentTime <= trail.time)
        {
            alpha = Mathf.Lerp(alpha, 0, currentTime / trail.time);
            trail.startColor = new Color(1, 1, 1, alpha);
            trail.endColor = new Color(1, 1, 1, alpha);
            currentTime += Time.deltaTime;
            yield return null;
        }
    }
}
