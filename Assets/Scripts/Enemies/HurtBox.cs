using System;
using UnityEngine;

public class HurtBox : MonoBehaviour, IDamageable, IPushable
{
    public event Action<int> OnDamage;
    public event Action OnHeadshot;
    [SerializeField]
    private int MultiplicadorDaņo;

    public void Damage(int amount)
    {
        OnDamage?.Invoke(amount * MultiplicadorDaņo);

        if (gameObject.tag == "Head")
        {
            OnHeadshot?.Invoke();
        }
    }

    public void Push(Vector3 direction, float impulse)
    {
        this.gameObject.GetComponent<Rigidbody>().AddForce(direction * impulse, ForceMode.Impulse);
    }
}
