using System;
using UnityEngine;

public interface IDamageable
{
    public event Action<int> OnDamage;
    void Damage(int amount);
}

public interface IPushable
{
    void Push(Vector3 direction, float impulse);
}
public interface IReactToSound
{
    void ReactToSound(Sound sound);
}


