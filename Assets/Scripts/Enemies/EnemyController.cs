using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour, IDamageable, IReactToSound
{
    public event Action<int> OnDamage;

    [Header("State Machine")]
    [SerializeField]
    private SwitchMachineStates m_CurrentState; //estado actual de la maquina  
    private enum SwitchMachineStates { NONE, IDLE, WALK, SHOOT, PATRULLA, PUPA, DEAD }; //enum de los estados de la maquina de estados      
    [SerializeField]
    private SwitchMachineStates m_MemoryState;

    [Header("Shooting")]
    [SerializeField]
    //private bool shooting;
    private Vector3 m_ShootDirection;
    [SerializeField]
    private float m_CooldownTime;

    [Header("Stats")]
    [SerializeField]
    private float m_Speed = 3f;
    [SerializeField]
    private float m_TiempoAntesPatrulla;
    [SerializeField]
    private int m_Vida;
    [SerializeField]
    private bool invulnerable;

    [Header("Área Detección")]
    [SerializeField]
    float offsetForwardSphereDeteccion;
    Vector3 originSphereDeteccion;
    [SerializeField]
    float offsetForwardSphereShoot;
    Vector3 originSphereShoot;
    [SerializeField]
    // [Min(1)]
    private float m_RadioDeteccion;
    [SerializeField]
    // [Min(1)]
    private float m_RadioDisparo;

    [Header("LayerMasks")]
    [SerializeField]
    private LayerMask m_PlayerMask; //la del player
    [SerializeField]
    private LayerMask m_VisionMask; //la de los objetos que le tapan la vista

    [Header("BodyParts")]
    [SerializeField]
    private Rigidbody[] bodyParts;

    [Header("Animator")]
    [SerializeField]
    private Animator m_Animator;
    //[SerializeField]
    //private Animation[] listaAnimaciones;

    [Header("NavMesh")]
    [SerializeField]
    private String m_PatrolArea;
    [SerializeField]
    private NavMeshAgent m_NavMeshAgent;
    [SerializeField]
    private Vector3 m_Destination;
    [SerializeField]
    private Transform m_DestinationPlayer;
    [SerializeField]
    private float m_MinXMap;
    [SerializeField]
    private float m_MaxXMap;
    [SerializeField]
    private float m_MinYMap;
    [SerializeField]
    private float m_MaxYMap;
    [SerializeField]
    private float m_MinZMap;
    [SerializeField]
    private float m_MaxZMap;
    private Coroutine m_BuscarPatrullaCoroutine;
    [SerializeField]
    private bool m_OnOffLink = false;
    [SerializeField]
    private bool reaccionandoSonido = false;

    [Header("Animations")]
    [SerializeField]
    private float m_TransitionDuration;

    [Header("Eventos")]
    [SerializeField]
    private GameEvent m_Backstab;

    private void Awake()
    {

        foreach (Rigidbody part in bodyParts)
        {
            part.gameObject.GetComponent<HurtBox>().OnDamage += Damage;
            part.isKinematic = true;
        }

        m_NavMeshAgent = GetComponent<NavMeshAgent>();
    }

    void Start()
    {
        ChangeState(SwitchMachineStates.IDLE);
    }

    void Update()
    {
        Detectar();
        UpdateState();
    }

    //[SerializeField]
    //private Animation[] listaAnimaciones;

    public void Damage(int amount)
    {
        if (!invulnerable)
        {
            print(this.name + " HA PERDIDO " + amount + " DE VIDA");
            m_Vida -= amount;
            if (m_Vida <= 0 && m_CurrentState != SwitchMachineStates.DEAD)
            {
                ChangeState(SwitchMachineStates.DEAD);
            }
            invulnerable = true;
            ChangeState(SwitchMachineStates.PUPA);
        }

        if (amount == 5000)
            m_Backstab.Raise();
    }



    //----------------------------------[ M�QUINA DE ESTADOS }----------------------------------//
    private void ChangeState(SwitchMachineStates newState)
    {
        if (m_CurrentState == SwitchMachineStates.DEAD)
            return;
        if (newState == SwitchMachineStates.PUPA)
        {
            m_MemoryState = m_CurrentState;
        }
        ExitState();
        InitState(newState);
        //Debug.Log(newState);
    }


    private void InitState(SwitchMachineStates currentState)
    {
        m_CurrentState = currentState;
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                m_Animator.CrossFade("Holding Idle", m_TransitionDuration);
                m_NavMeshAgent.speed = 0;
                StartCoroutine(MeVoyPatrullar());
                break;
            case SwitchMachineStates.WALK:
                m_Animator.CrossFade("Walking", m_TransitionDuration);
                m_NavMeshAgent.speed = m_Speed;
                break;
            case SwitchMachineStates.SHOOT:
                m_Animator.CrossFade("Shooting", m_TransitionDuration);
                m_NavMeshAgent.speed = 0;
                transform.forward = new Vector3(m_ShootDirection.x, 0, m_ShootDirection.z); //Enemy mira a player
                StartCoroutine(Shoot());
                break;
            case SwitchMachineStates.PATRULLA:
                m_NavMeshAgent.speed = m_Speed;
                m_BuscarPatrullaCoroutine = StartCoroutine(BuscoPuntoDePatrulla());
                break;
            case SwitchMachineStates.PUPA:
                m_Animator.CrossFade("Pain Gesture", m_TransitionDuration);
                break;
            case SwitchMachineStates.DEAD:
                m_NavMeshAgent.isStopped = true;
                StopAllCoroutines();
                foreach (Rigidbody part in bodyParts)
                {
                    part.gameObject.GetComponent<HurtBox>().OnDamage -= Damage;
                    part.isKinematic = false;
                }
                m_Animator.enabled = false;
                break;
            default:
                break;
        }
    }

    private void UpdateState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:

                break;
            case SwitchMachineStates.WALK:
                //StopAllCoroutines();
                m_NavMeshAgent.SetDestination(m_DestinationPlayer.position);
                OnOffLink();
                ARango();
                break;
            case SwitchMachineStates.SHOOT:
                break;
            case SwitchMachineStates.PATRULLA:
                if (m_NavMeshAgent.remainingDistance <= 1 && m_Destination != Vector3.zero)
                {
                    m_Destination = Vector3.zero;
                    ChangeState(SwitchMachineStates.IDLE);
                }
                OnOffLink();
                break;
            default:
                break;
        }
    }

    private void ExitState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.SHOOT:
                StopAllCoroutines();
                break;
            case SwitchMachineStates.PATRULLA:
                StopCoroutine(m_BuscarPatrullaCoroutine);
                break;
            case SwitchMachineStates.PUPA:
                invulnerable = false;
                m_MemoryState = SwitchMachineStates.NONE;
                break;
            default:
                break;
        }
    }

    private void Detectar()
    {
        //RaycastHit[] objetivos;
        originSphereDeteccion = transform.position + transform.forward * offsetForwardSphereDeteccion;
        Collider[] objetivo = Physics.OverlapSphere(originSphereDeteccion, m_RadioDeteccion, m_PlayerMask);
        if (objetivo.Length > 0 && m_CurrentState != SwitchMachineStates.WALK)
        {
            RaycastHit hit;
            if (Physics.Raycast(this.transform.position, (objetivo[0].transform.position - this.transform.position).normalized, out hit, Mathf.Infinity, m_VisionMask))
            {
                if (hit.collider.gameObject.CompareTag("Player") && m_CurrentState != SwitchMachineStates.SHOOT && m_CurrentState != SwitchMachineStates.PUPA)
                {
                    m_DestinationPlayer = hit.collider.gameObject.transform;
                    ChangeState(SwitchMachineStates.WALK);
                }
                else if (m_CurrentState == SwitchMachineStates.PUPA)
                {
                    m_NavMeshAgent.speed = 0;
                }
            }
        }
        else if (objetivo.Length <= 0 && m_CurrentState == SwitchMachineStates.WALK && !reaccionandoSonido)
        {
            ChangeState(SwitchMachineStates.IDLE);
        }

    }

    private void ARango()
    {
        originSphereShoot = transform.position + transform.forward * offsetForwardSphereShoot;
        Collider[] objetivo = Physics.OverlapSphere(originSphereShoot, m_RadioDisparo, m_PlayerMask);

        if (objetivo.Length > 0)
        {
            m_ShootDirection = (objetivo[0].transform.position - this.transform.position).normalized;
            //shooting = true;
            ChangeState(SwitchMachineStates.SHOOT);

        }
        else
        {
            //shooting = false;
            ChangeState(SwitchMachineStates.WALK);
        }
    }

    private IEnumerator Shoot()
    {
        RaycastHit hit;
        if (Physics.Raycast((transform.position + new Vector3(0, 1, 0)), m_ShootDirection, out hit, m_RadioDisparo, m_VisionMask))
        {
            Debug.DrawLine((transform.position + new Vector3(0, 1, 0)), hit.point, Color.yellow, 2f);
            if (hit.collider.gameObject.CompareTag("Player"))
            {
                if (hit.collider.TryGetComponent(out IDamageable target))
                    target.Damage(5);
            }
        }
        yield return new WaitForSeconds(m_CooldownTime);
        ARango();
    }

    private IEnumerator MeVoyPatrullar()
    {
        yield return new WaitForSeconds(m_TiempoAntesPatrulla);
        ChangeState(SwitchMachineStates.PATRULLA);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(originSphereDeteccion, m_RadioDeteccion);

        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(originSphereShoot, m_RadioDisparo);
    }

    private IEnumerator BuscoPuntoDePatrulla()
    {
        yield return new WaitForSeconds(m_TiempoAntesPatrulla);
        int area = 1 << NavMesh.GetAreaFromName(m_PatrolArea);
        NavMeshHit hit;
        while (!NavMesh.SamplePosition(new Vector3(UnityEngine.Random.Range(m_MinXMap, m_MaxXMap), UnityEngine.Random.Range(m_MinYMap, m_MaxYMap), UnityEngine.Random.Range(m_MinZMap, m_MaxZMap)), out hit, 0.3f, area))
        {
            yield return null;

        }

        m_Destination = hit.position;
        m_NavMeshAgent.SetDestination(m_Destination);
        m_Animator.CrossFade("Walking", m_TransitionDuration);
    }


    public void ReturnToMemoryState()
    {
        ChangeState(m_MemoryState);
    }

    private void OnOffLink()
    {
        if (m_NavMeshAgent.enabled && m_NavMeshAgent.isOnOffMeshLink)
        {
            m_OnOffLink = true;
            OffMeshLinkData link = m_NavMeshAgent.currentOffMeshLinkData;
            if (link.offMeshLink != null)
            {
                if (link.offMeshLink.area == NavMesh.GetAreaFromName("Climb"))
                {
                    m_Animator.CrossFade("Climbing Ladder", m_TransitionDuration);
                }
                else if (link.offMeshLink.area == NavMesh.GetAreaFromName("Jump"))
                {
                    m_Animator.CrossFade("Jumping", m_TransitionDuration);
                }
            }
        }
        else if (!m_NavMeshAgent.isOnOffMeshLink && m_OnOffLink)
        {
            m_Animator.CrossFade("Walking", m_TransitionDuration);
            m_OnOffLink = false;
        }
    }

    public void ReactToSound(Sound sound)
    {
        if (m_CurrentState != SwitchMachineStates.DEAD && m_CurrentState != SwitchMachineStates.SHOOT)
        {
            reaccionandoSonido = true;
            m_DestinationPlayer = sound.soundPosition;
            m_NavMeshAgent.SetDestination(m_DestinationPlayer.position);
            StartCoroutine(Reaccionando());
            ChangeState(SwitchMachineStates.WALK);
        }
    }

    private IEnumerator Reaccionando()
    {
        float tiempo = 0;
        while (m_NavMeshAgent.remainingDistance > m_RadioDeteccion && m_Destination != Vector3.zero && tiempo <= 10)
        {
            tiempo += Time.deltaTime;
            yield return null;
        }
        reaccionandoSonido = false;
        m_Destination = Vector3.zero;
    }
}
