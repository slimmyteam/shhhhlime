using UnityEngine;

public class RagdollControllerEnemy : MonoBehaviour
{
    [Header("Enemy")]
    [SerializeField]
    private GameObject m_Enemy;

    public void TerminarAnimacionYVolverAMemoryState()
    {
        m_Enemy.GetComponent<EnemyController>().ReturnToMemoryState();
    }
}
